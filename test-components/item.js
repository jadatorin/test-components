import {html, css, LitElement} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

export class Item extends LitElement {
	static properties = {
		product: {type: Object},
	}


	static styles = css`
		.row {
			display: flex;
			border: 5px solid orange;
			padding: 10px;
		}
		.row .column {
			flex: 1;
		}

	`

	constructor() {
		super();
		this.product = {};
	}

	render() {
		return html`
		<div class="row">
			<div class="column">producto: ${this.product.name}</div>
			<div class="column">
			     <button @click=${this.deleteProduct}>x</button>
			</div>
		</div>
		`;
	}

	deleteProduct(sourceEvent) {
		console.log('ITEM :::: Ya estamos avanzando... al menos un click');
		const options = {
			detail: this.product,
		};
		const myEvent = new CustomEvent('deleteProduct', options);
		this.dispatchEvent(myEvent);
	}
}

customElements.define('ceco-item', Item)


