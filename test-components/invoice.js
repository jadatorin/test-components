import {html, css, LitElement} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

export class Invoice extends LitElement {
	static properties = {
		products: {type: Array},
	}

	constructor() {
		super();
		this.products = [];
	}

	render() {
		return html`
		<div style="border: 10px solid red">
		    <small>yo soy ceco-invoice</small>
		    <div>products=${this.products.length}</div>
	    	${this.products.map(product => 
	    		html`
	    		<ceco-item .product=${product} @deleteProduct="${this.deleteProduct}"></ceco-item>
	    		`
	    	)}
		</div>
		`;
	}
	deleteProduct(event) {
		console.log('INVOICE :::: Se emitió el evento de deleteProduct:', event);
		const newEvent = new CustomEvent('deleteProduct', event);
		this.dispatchEvent(newEvent);
	}
}



customElements.define('ceco-invoice', Invoice)


