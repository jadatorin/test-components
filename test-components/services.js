/* Repositorio con la lista de productos */
let products = [];


export const srvFindProduct = async (code) => {
	const product = {
		// Creamos un id aleatorio
		internalId: Math.round(Math.random() * (100 - 1) + 1),
		name: code
	};
	return product;
};


export const srvAddProduct = async (product) => {
	products = products.concat([product]);
	return products;
};


export const srvDeleteProduct = async (productToDelete) => {
	products = products.filter(product => product.internalId !== productToDelete.internalId);
	return products;
};


export const srvGetProducts = async () => {
	return products;
};