import {
	html,
	css,
	LitElement
} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

import {
	srvFindProduct,
	srvAddProduct,
	srvDeleteProduct,
	srvGetProducts
} from './services.js';


export class App extends LitElement {

	static properties = {
		title: {type: String},
	}

	constructor() {
		super();
		this.products = [];
		this.title = "Feria del Centro";
	}

	render() {
		return html`
		   <div style="border: 5px solid blue">
		    <small>yo soy ceco-app</small>
  			<input type="text" @change="${this.addProduct}">
  			<hr/>
  			<ceco-invoice .products="${this.products}" @deleteProduct="${this.processDeleteProduct}"></ceco-invoice>
  		  </div>
		`;
	}

	async firstUpdated() {
		const products = await srvGetProducts();
		this.products = products;
	}

	/*
	 * Función para agregar un producto
	*/
	async addProduct(event) {
		const input = event.target;
		const productId = input.value;
		const product = await srvFindProduct(productId);
		const products = await srvAddProduct(product);
		this.products = products;
		this.requestUpdate();
	}

	async processDeleteProduct(event) {
		console.log('APP :::: Se emitió el evento de deleteProduct:', event);
		const productToDelete = event.detail;
		this.products = await srvDeleteProduct(productToDelete);
		this.requestUpdate();
	}

}



customElements.define('ceco-app', App);
